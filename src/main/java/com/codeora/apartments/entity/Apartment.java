package com.codeora.apartments.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Entity
@Table(name = "apartment")
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
public class Apartment {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "zip_code", nullable = false)
    private String zipCode;
    @NotNull
    @Column(name = "street", nullable = false)
    private String street;
    @NotNull
    @Column(name = "street_no", nullable = false)
    private String streetNo;
    @NotNull
    @Column(name = "area", nullable = false)
    private double area;
    @NotNull
    @Column(name = "price", nullable = false)
    private double price;
    @NotNull
    @Column(name = "floor", nullable = false)
    private int floor;
    @Column(name = "enabled", nullable = false, columnDefinition = "TINYINT(1)")
    private Boolean enabled = true;

    @Version @Temporal(TemporalType.TIMESTAMP)
    @Column(name="version", nullable = false)
    private Date version;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public Date getVersion() {
        return version;
    }

    public Long getId() {
        return id;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
