package com.codeora.apartments.service;

import com.codeora.apartments.entity.Apartment;
import com.codeora.apartments.pojo.ApartmentSearchQuery;
import com.codeora.util.QueryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class ApartmentService {
    @PersistenceContext
    private EntityManager em;

    @Inject
    private QueryService queryService;

    public Apartment save(Apartment apartment) {
        return this.em.merge(apartment);
    }

    public List<Apartment> search(ApartmentSearchQuery query) {
        try {
            return queryService.list(query, Apartment.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Apartment getApartmentById(long id) {
        return em.find(Apartment.class, id);
    }

    public Apartment updateApartment(Apartment apartment) {
        if (apartment.getId() == null) {
            return null;
        }
        Apartment target = getApartmentById(apartment.getId());
        if (target == null) {
            return null;
        }
        target.setFloor(apartment.getFloor());
        target.setArea(apartment.getArea());
        target.setEnabled(apartment.getEnabled());
        target.setPrice(apartment.getPrice());
        target.setStreet(apartment.getStreet());
        target.setStreetNo(apartment.getStreetNo());
        target.setZipCode(apartment.getZipCode());
        return em.merge(target);
    }

    public void deleteApartmentById(Long id) {
        Apartment a = em.getReference(Apartment.class, id);
        a.setEnabled(false);
    }
}
