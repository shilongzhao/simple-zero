package com.codeora.apartments.boundary;

import com.codeora.apartments.entity.Apartment;
import com.codeora.apartments.pojo.ApartmentSearchQuery;
import com.codeora.apartments.service.ApartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/apartments")
@Api(tags = "apartments")
@Produces(MediaType.APPLICATION_JSON) @Consumes(MediaType.APPLICATION_JSON)
public class ApartmentResource {
    private static final Logger log = Logger.getLogger(ApartmentResource.class.getCanonicalName());
    @Inject
    private ApartmentService as;

    @POST
    @ApiOperation(value = "Create apartment", response = Apartment.class)
    public Response create(@Valid Apartment apartment) {
        Apartment saved = as.save(apartment);
        return Response.ok(saved).build();
    }

    @POST
    @Path("/search")
    @ApiOperation(value = "Get apartment by search queries", response = Apartment.class, responseContainer = "List")
    public Response search(@Valid ApartmentSearchQuery searchQuery) {
        List<Apartment> r = as.search(searchQuery);
        log.log(Level.INFO, "found apartments: " + r.size());
        return Response.ok(r).build();
    }

    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get apartment by ID", response = Apartment.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Apartment not found")
    })
    public Response getApartmentById(@PathParam("id") long id) {
        Apartment a = as.getApartmentById(id);
        if (a == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(a).build();
    }

    @PUT
    @Path("/{id}")
    @ApiOperation(value = "Update apartment by ID", response = Apartment.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Apartment not found")
    })
    public Response updateApartment(@PathParam("id") long id, @Valid Apartment apartment) {
        Apartment updated = as.updateApartment(apartment);
        if (updated == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(updated).build();
    }

    @POST
    @Path("/{id}")
    @ApiOperation(value = "Unsupported action")
    @ApiResponses(value = {
            @ApiResponse(code = 405, message = "Operation not supported")
    })
    public Response notAllowed(@PathParam("id") Long id) {
        return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
    }

    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Update apartment by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation successful")
    })
    public Response deleteApartmentById(@PathParam("id") Long id) {
        as.deleteApartmentById(id);
        return Response.ok().build();
    }

}
