package com.codeora.apartments.pojo;

import com.codeora.apartments.entity.Apartment;
import com.codeora.util.QueryOperator;
import com.codeora.util.QueryCondition;
import com.codeora.util.QueryWrap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ApartmentSearchQuery implements QueryWrap<Apartment> {
    @QueryCondition(target = "id", operator = QueryOperator.EQUAL)
    private Long id;
    @QueryCondition(target = "area", operator = QueryOperator.GE)
    private Double areaMin;
    @QueryCondition(target = "area", operator = QueryOperator.LE)
    private Double areaMax;
    @QueryCondition(target = "price", operator = QueryOperator.GE)
    private Double priceMin;
    @QueryCondition(target = "price", operator = QueryOperator.LE)
    private Double priceMax;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAreaMin() {
        return areaMin;
    }

    public void setAreaMin(Double areaMin) {
        this.areaMin = areaMin;
    }

    public Double getAreaMax() {
        return areaMax;
    }

    public void setAreaMax(Double areaMax) {
        this.areaMax = areaMax;
    }

    public Double getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Double priceMin) {
        this.priceMin = priceMin;
    }

    public Double getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(Double priceMax) {
        this.priceMax = priceMax;
    }
}
