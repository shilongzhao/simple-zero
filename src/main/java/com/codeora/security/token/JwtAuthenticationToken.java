package com.codeora.security.token;

import org.apache.shiro.authc.AuthenticationToken;

public class JwtAuthenticationToken implements AuthenticationToken {
    private JwtTokenUserSubject user;

    private String token;

    public JwtAuthenticationToken() {

    }

    public JwtAuthenticationToken(JwtTokenUserSubject user, String token) {
        this.user = user;
        this.token = token;
    }

    public JwtTokenUserSubject getUser() {
        return user;
    }

    public void setUser(JwtTokenUserSubject user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
