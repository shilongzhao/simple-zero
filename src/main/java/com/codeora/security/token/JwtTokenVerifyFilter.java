package com.codeora.security.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.codeora.configuration.Conf;
import com.codeora.configuration.ConfigurationService;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Locale;
import java.util.logging.Logger;

public class JwtTokenVerifyFilter extends AuthenticatingFilter {
    private static final Logger log = Logger.getLogger(JwtTokenVerifyFilter.class.getCanonicalName());

    protected static final String AUTHORIZATION_HEADER = "Authorization";

    private String authzScheme = "Bearer";

    private ConfigurationService configurationService;

    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        // NOTE: this filter in the Shiro filter hierarchy does not have access to the ServletContext!
        JWTVerifier verifier = JWT.require(Algorithm.HMAC512(configurationService.getValue(Conf.JWT_TOKEN_SECRET)))
                .acceptLeeway(1)   //1 sec for nbf and iat
                .acceptExpiresAt(5)
                .build();
        String authzHeader = getAuthzHeader(request);
        if (authzHeader == null) {
            throw new JWTVerificationException("Invalid Http header");
        }
        String token = authzHeader.substring(authzScheme.length()).trim();

        DecodedJWT decodedJWT = verifier.verify(token);
        JwtTokenUserSubject user = new JwtTokenUserSubject(decodedJWT.getSubject());
        /**
         * TODO:
         * the name must be same with what is set in  {@link com.codeora.security.control.UserService#issueToken(String)}
         */
        String[] perms = decodedJWT.getClaim("perms").asArray(String.class);
        String[] roles = decodedJWT.getClaim("roles").asArray(String.class);

        user.getPerms().addAll(Arrays.asList(perms));
        user.getRoles().addAll(Arrays.asList(roles));
        /**
         * this Jwt Auth token will be handled to
         * {@link com.codeora.security.token.JwtTokenRealm#doGetAuthenticationInfo(AuthenticationToken)}
         */
        return new JwtAuthenticationToken(user, token);
    }

    /**
     * If {@link #isAccessAllowed(ServletRequest, ServletResponse, Object)} returns false,
     * then this method will be called. In our app, on each request, this method will be called,
     * since our app is stateless and subject is not remembered at all.
     * @param request Http Request
     * @param response Http Response
     * @return
     * @throws Exception
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {

        boolean loggedIn = false;
        /**
         * if the request header contains the Authorization header, we try to login:
         *  if the token is valid (not expired, not illegally modified, etc), the access is allowed
         *  otherwise, access will be denied
         *
         */
        if (isLoginAttempt(request, response)) {
            loggedIn = executeLogin(request, response);
        }
        if (!loggedIn) {
            // TODO: put an exception case here may not be the best solution
            if (isOptionsRequest(request, response)) {
                return true;
            }
            HttpServletResponse httpServletResponse = WebUtils.toHttp(response);
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        return loggedIn;
    }

    protected String getAuthzHeader(ServletRequest request) {
        HttpServletRequest httpRequest = WebUtils.toHttp(request);
        return httpRequest.getHeader(AUTHORIZATION_HEADER);
    }

    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        String authzHeader = getAuthzHeader(request);
        return authzHeader != null && isLoginAttempt(authzHeader);
    }

    private boolean isLoginAttempt(String authzHeader) {
        String authzScheme = getAuthzScheme().toLowerCase(Locale.ENGLISH);
        return authzHeader.toLowerCase(Locale.ENGLISH).startsWith(authzScheme);
    }

    public String getAuthzScheme() {
        return authzScheme;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    // check if it is OPTIONS http request, which is used for Cross Origin Request
    private boolean isOptionsRequest(ServletRequest request, ServletResponse response) {
        HttpServletRequest httpRequest = WebUtils.toHttp(request);
        return httpRequest.getMethod().equals("OPTIONS");
    }
}
