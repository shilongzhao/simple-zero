package com.codeora.security.token;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JwtTokenUserSubject {
    private String username;
    private Set<String> roles;
    private Set<String> perms;

    public JwtTokenUserSubject(String username) {
        this.username = username;
        this.roles = new HashSet<>();
        this.perms = new HashSet<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getRoles() {
        return roles;
    }


    public Set<String> getPerms() {
        return perms;
    }

}
