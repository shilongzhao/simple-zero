package com.codeora.security.token;

import com.codeora.security.control.UserService;
import com.codeora.security.entity.User;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.logging.Logger;

public class JwtTokenRealm extends AuthorizingRealm {
    private static final Logger logger = Logger.getLogger(JwtTokenRealm.class.getCanonicalName());

    private UserService userService;

    public JwtTokenRealm() {
        super();
        setAuthenticationCachingEnabled(Boolean.TRUE);

        try {
            InitialContext ctx = new InitialContext();
            String moduleName = (String) ctx.lookup("java:module/ModuleName");
            this.userService = (UserService) ctx.lookup(String.format("java:global/%s/UserService", moduleName));
        } catch (NamingException ex) {
            logger.warning("Cannot do the JNDI Lookup to instantiate the User service : " + ex);
        }
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return (token != null) && (token instanceof JwtAuthenticationToken);
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
            throws AuthenticationException {
        JwtAuthenticationToken jwtToken = (JwtAuthenticationToken) token;
        JwtTokenUserSubject userSubject = (JwtTokenUserSubject) jwtToken.getPrincipal();
        if (userSubject == null) {
            logger.warning("Username is null.");
            throw new IncorrectCredentialsException();
        }
        // we just return exactly the same token as the credential,
        // meaning that the token will always be accepted by the CredentialMatcher
        return new SimpleAuthenticationInfo(userSubject, jwtToken.getToken(), getName());
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        /**
         * we get the authorization info directly from the user token.
         */
        JwtTokenUserSubject userSubject = principals.oneByType(JwtTokenUserSubject.class);
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(userSubject.getRoles());
        info.setStringPermissions(userSubject.getPerms());
        return info;
    }

}
