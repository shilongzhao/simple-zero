package com.codeora.security.configuration;

import com.codeora.security.control.UserService;
import com.codeora.security.entity.User;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * The SecurityRealm is only used when user tries to login at /api/auth/login: where a UsernamePasswordToken is generated
 * by FormAuthenticationFilter createToken.
 * This token is passed to doGetAuthenticationInfo, and from DB the
 * username and password is collected. Then AuthenticationInfo returned
 * will be compared with information received from request by the
 * {@link org.apache.shiro.authc.credential.CredentialsMatcher CredentialMatcher}
 * set in {@link com.codeora.security.configuration.ShiroConfiguration ShiroConfiguration} .
 */
public class SecurityRealm extends AuthorizingRealm {

    private Logger logger;
    private UserService userService;

    public SecurityRealm() {
        super();
        this.logger = Logger.getLogger(SecurityRealm.class.getName());

        setAuthenticationCachingEnabled(Boolean.TRUE);

        try {
            InitialContext ctx = new InitialContext();
            String moduleName = (String) ctx.lookup("java:module/ModuleName");
            this.userService = (UserService) ctx.lookup(String.format("java:global/%s/UserService", moduleName));
        } catch (NamingException ex) {
            logger.warning("Cannot do the JNDI Lookup to instantiate the User service : " + ex);
        }
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return (token != null) && (token instanceof UsernamePasswordToken);
    }
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        // identify account to log to
        UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
        String username = userPassToken.getUsername();

        if (username == null) {
            logger.warning("Username is null.");
            return null;
        }

        // read password hash and salt from db
        User user = this.userService.findByUsername(username);

        if (user == null) {
            logger.warning("No account found for user [" + username + "]");
            throw new IncorrectCredentialsException();
        }

        return new SimpleAuthenticationInfo(username, user.getPassword(), getName());
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        //null usernames are invalid
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }

        String username = principals.oneByType(String.class);

        Set<String> roleNames = new HashSet<>();

        roleNames.addAll(userService.getRolesByUsername(username));

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
        info.addStringPermissions(userService.getPermissionsByUsername(username));
        /**
         * If you want to do Permission Based authorization, you can grab the Permissions List associated to your user:
         * For example:
         * Set<String> permissions = new HashSet<>();
         * permissions.add(this.userService.findByUsername(username).getRole().getPermissions());
         * ((SimpleAuthorizationInfo)info).setStringPermissions(permissions);
         */
        return info;
    }

}

