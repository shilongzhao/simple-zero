package com.codeora.security.configuration;

import com.codeora.configuration.ConfigurationService;
import com.codeora.security.token.JwtTokenRealm;
import com.codeora.security.token.JwtTokenVerifyFilter;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.filter.authc.AnonymousFilter;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.apache.shiro.web.filter.mgt.FilterChainResolver;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.filter.session.NoSessionCreationFilter;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.util.Arrays;

/**
 */
public class ShiroConfiguration {
    @Inject
    private ConfigurationService configurationService;

    @Produces
    public WebSecurityManager getSecurityManager() {
        DefaultWebSecurityManager securityManager = null;

        AuthorizingRealm realm = new SecurityRealm();

        CredentialsMatcher credentialsMatcher = new PasswordMatcher();
        ((PasswordMatcher) credentialsMatcher).setPasswordService(new BCryptPasswordService());
        realm.setCredentialsMatcher(credentialsMatcher);

        JwtTokenRealm tokenRealm = new JwtTokenRealm();
        tokenRealm.setCredentialsMatcher(new SimpleCredentialsMatcher());

        CacheManager cacheManager = new EhCacheManager();
        ((EhCacheManager) cacheManager).setCacheManagerConfigFile("classpath:ehcache.xml");

        if (securityManager == null) {
            securityManager = new DefaultWebSecurityManager(Arrays.asList(realm, tokenRealm));
            securityManager.setCacheManager(cacheManager);
        }
        return securityManager;
    }

    @Produces
    public FilterChainResolver getFilterChainResolver() {
        FilterChainResolver filterChainResolver = null;
        if (filterChainResolver == null) {

            AnonymousFilter anon = new AnonymousFilter();
            JwtTokenVerifyFilter jwt = new JwtTokenVerifyFilter();
            jwt.setConfigurationService(configurationService);
            NoSessionCreationFilter noSessionCreation = new NoSessionCreationFilter();

            FilterChainManager fcMan = new DefaultFilterChainManager();

            fcMan.addFilter("anon", anon);
            fcMan.addFilter("noSessionCreation", noSessionCreation);
            fcMan.addFilter("jwt", jwt);

            // TODO: html, css will be deleted
            fcMan.createChain("/api/auth/login", "noSessionCreation, anon");
            fcMan.createChain("/**", "noSessionCreation, jwt");

            PathMatchingFilterChainResolver resolver = new PathMatchingFilterChainResolver();
            resolver.setFilterChainManager(fcMan);
            filterChainResolver = resolver;
        }
        return filterChainResolver;
    }

}
