package com.codeora.security.configuration;

import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.web.mgt.CookieRememberMeManager;

/**
 * @author <a href="mailto:s.zhao@opengate.biz">Shilong Zhao</a>
 */
public class RandomRememberMeManager
        extends CookieRememberMeManager {

    /**
     * Default constructor.  Sets a new random cipher key.
     */
    public RandomRememberMeManager() {
        super();

        AesCipherService aesCipherService = new AesCipherService();
        setCipherService(aesCipherService);
        setCipherKey(aesCipherService.generateNewKey().getEncoded());

    }

}