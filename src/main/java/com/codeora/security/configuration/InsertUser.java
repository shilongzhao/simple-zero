package com.codeora.security.configuration;

import com.codeora.security.control.AuthorizationService;
import com.codeora.security.control.UserService;
import com.codeora.security.entity.Permission;
import com.codeora.security.entity.Role;
import com.codeora.security.entity.User;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.logging.Logger;

/**
 */
@Singleton
@Startup
public class InsertUser {
    @Inject
    UserService us;
    @Inject
    BCryptPasswordService passwordService;
    @Inject
    AuthorizationService as;

    private Logger LOGGER = Logger.getLogger(InsertUser.class.getName());

    @PostConstruct
    public void insert() {

        User s = new User("shilong", passwordService.encryptPassword("123456"));
        s.setEmail("zhao.s.long@gmail.com");

        Role roleAdmin = as.save(new Role("ADMIN"));
        Role roleCustomer = as.save(new Role("CUSTOMER"));

        Permission usersRead = as.save(new Permission("users:read"));
        Permission usersCreate = as.save(new Permission("users:create"));
        Permission usersDelete = as.save(new Permission("users:delete"));
        Permission usersUpdate = as.save(new Permission("users:update"));

        s = this.us.save(s);

        as.assign(s, roleAdmin);
        as.assign(roleAdmin, usersDelete);
        as.assign(s, usersUpdate);
        as.assign(s, usersCreate);
        as.assign(s, usersRead);
        LOGGER.warning("######################");
    }
}
