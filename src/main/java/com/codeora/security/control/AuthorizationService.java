package com.codeora.security.control;

import com.codeora.security.entity.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * author: zhaoshilong
 * date: 15/11/2017
 */
@Stateless
public class AuthorizationService {
    @PersistenceContext
    private EntityManager em;

    public Role save(Role role) {
        Role existing = null;
        try {
            existing = em.createQuery("" +
                    " SELECT r " +
                    " FROM Role r " +
                    " WHERE r.name = :name", Role.class)
                    .setParameter("name", role.getName())
                    .getSingleResult();
        } catch (NoResultException e) {
            return em.merge(role);
        }
        return existing;
    }

    public Permission save(Permission permission) {
        Permission existing = null;
        try {
            existing = em.createQuery("" +
                    " SELECT P FROM Permission p " +
                    " WHERE p.code = :code ", Permission.class)
                    .setParameter("code", permission.getCode())
                    .getSingleResult();
        } catch (NoResultException e) {
            return em.merge(permission);
        }
        return existing;
    }

    /**
     * add user to role
     * @param user
     * @param role
     * @return
     */
    public UserRole assign(User user, Role role) {
        UserRole existing = null;
        try {
            existing = em.createQuery("" +
                    "SELECT ur FROM UserRole ur " +
                    "   JOIN FETCH ur.user u " +
                    "   JOIN FETCH ur.role r " +
                    " WHERE u.username = :username " +
                    "   AND r.name = :roleName ", UserRole.class)
                    .setParameter("username", user.getUsername())
                    .setParameter("roleName", role.getName())
                    .getSingleResult();
        } catch (NoResultException e) {
            UserRole userRole = new UserRole(user, role);
            return em.merge(userRole);
        }
        existing.setEnabled(true);
        return existing;
    }

    /**
     * Assign permission to user
     * @param user
     * @param perm
     * @return
     */
    public UserPermission assign(User user, Permission perm) {
        UserPermission existing = null;
        try {
            existing = em.createQuery("" +
                    " SELECT up FROM UserPermission up " +
                    "   JOIN FETCH up.user u " +
                    "   JOIN FETCH up.permission p " +
                    " WHERE u.username = :username " +
                    "   AND p.code = :code ", UserPermission.class)
                    .setParameter("username", user.getUsername())
                    .setParameter("code", perm.getCode())
                    .getSingleResult();
        } catch (NoResultException e) {
            UserPermission up = new UserPermission(user, perm);
            return em.merge(up);
        }
        existing.setEnabled(true);
        return existing;
    }

    public int disable(User user, Permission perm) {
        int count = em.createQuery("" +
                " UPDATE UserPermission up " +
                " SET up.enabled = false " +
                " WHERE up.user.username = :username " +
                "   AND up.permission.code = :code ")
                .setParameter("username", user.getUsername())
                .setParameter("code", perm.getCode())
                .executeUpdate();
        return count;
    }
    /**
     * Assign a permission to a role
     * @param role
     * @param perm
     * @return
     */
    public RolePermission assign(Role role, Permission perm) {
        RolePermission exist = null;
        try {
            exist = em.createQuery("" +
                    " SELECT rp FROM RolePermission rp " +
                    "   JOIN rp.role r " +
                    "   JOIN rp.permission p " +
                    " WHERE r.name = :roleName " +
                    "   AND p.code = :permCode ", RolePermission.class)
                    .setParameter("roleName", role.getName())
                    .setParameter("permCode", perm.getCode())
                    .getSingleResult();
        } catch (NoResultException e) {
            RolePermission rp = new RolePermission(role, perm);
            return em.merge(rp);
        }
        exist.setEnabled(true);
        return exist;
    }


}
