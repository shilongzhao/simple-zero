package com.codeora.security.control;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.codeora.configuration.Conf;
import com.codeora.configuration.ConfigurationService;
import com.codeora.security.configuration.WebPages;
import com.codeora.security.entity.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 */
@Stateless
public class UserService {
    @PersistenceContext
    EntityManager em;

    @Inject
    private Logger logger;

    @Inject
    private ConfigurationService confService;

    public User save(User user) {
        return this.em.merge(user);
    }

    public User findById(long id) {
        return this.em.find(User.class, id);
    }

    public List<User> findAll() {
        return this.em
                .createNamedQuery("User.findAll", User.class)
                .getResultList();
    }

    public User findByUsername(String username) {
        User result = null;
        try {
            result = this.em.createNamedQuery("User.findByUsername", User.class)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            logger.info("UserService : No valid User was found for [" + username + "] : " + e);
        } finally {
            return result;
        }
    }

    public User findByEmail(String email) {
        List<User> users = em.createQuery(
                " SELECT u " +
                        " FROM User u " +
                        " WHERE u.email = :email", User.class)
                .setParameter("email", email).getResultList();
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }

    /**
     * Disable user instead of delete
     * @param id user ID
     */
    public void delete(long id) {
        User userReference;
        try {
            userReference = this.em.getReference(User.class, id);
            userReference.setEnabled(false);
        } catch (EntityNotFoundException e) {
            logger.finest("User with id[" + id + "] was not delete because it doesn't exist." + e);
        }
    }

    public List<String> getRolesById(long id) {
        List<String> roles = em.createQuery(" " +
                " SELECT r.name FROM UserRole ur" +
                "   JOIN ur.role r " +
                "   JOIN ur.user u " +
                " WHERE u.id = :id ", String.class)
                .setParameter("id", id)
                .getResultList();
        return roles;
    }

    public List<String> getRolesByUsername(String username) {
        List<String> roles = em.createQuery("" +
                " SELECT r.name FROM UserRole ur " +
                "   LEFT JOIN ur.user u " +
                "   LEFT JOIN ur.role r " +
                " WHERE u.username = :username ", String.class)
                .setParameter("username", username)
                .getResultList();
        return roles;
    }

    /**
     * return all permissions associated with user & her roles
     * @param username
     * @return
     */
    public List<String> getPermissionsByUsername(String username) {
        List<String> userPerms = em.createQuery("" +
                " SELECT p.code FROM UserPermission up " +
                "   LEFT JOIN up.user u " +
                "   LEFT JOIN up.permission p " +
                " WHERE u.username = :username ", String.class)
                .setParameter("username", username)
                .getResultList();

        List<String> userRolePerms = em.createQuery("" +
                " SELECT p.code FROM RolePermission rp " +
                "   LEFT JOIN rp.permission p " +
                "   LEFT JOIN rp.role r " +
                "   WHERE r.id IN (" +
                "       SELECT r.id FROM UserRole ur " +
                "            LEFT JOIN ur.user u " +
                "            LEFT JOIN ur.role r " +
                "        WHERE u.username = :username" +
                "   ) ", String.class).setParameter("username", username)
                .getResultList();
        userPerms.addAll(userRolePerms);
        return userPerms;
    }

    /**
     * Generate JWT
     * @param username user to generate
     * @return JSON Web Token which contains username, roles, permissions, token expiration time
     */
    public String issueToken(String username)  {
        List<String> roles = getRolesByUsername(username);
        List<String> perms = getPermissionsByUsername(username);
        String token = null;
        try {
            token = JWT.create()
                    .withSubject(username)
                    .withArrayClaim("roles",  roles.toArray(new String[roles.size()]))
                    .withArrayClaim("perms", perms.toArray(new String[perms.size()]))
                    .withIssuedAt(new Date())
                    //TODO: token lifetime should be configurable
                    .withExpiresAt(toDate(LocalDateTime.now().plusMinutes(720L)))
                    .sign(Algorithm.HMAC512(confService.getValue(Conf.JWT_TOKEN_SECRET)));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        logger.info("#### generating token for user: " + username);
        return token;
    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}

