package com.codeora.security.boundary;

import com.codeora.apartments.entity.Apartment;
import com.codeora.security.configuration.BCryptPasswordService;
import com.codeora.security.configuration.WebPages;
import com.codeora.security.control.AuthenticationEventMonitor;
import com.codeora.security.control.UserService;
import com.codeora.security.entity.AuthenticationEvent;
import com.codeora.security.entity.User;
import com.codeora.security.token.JwtToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes({MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_FORM_URLENCODED})
@Api(tags = "auth")
public class AuthenticationResource {

    @Inject
    private Logger logger;

    @Inject
    private UserService userService;

    @Inject
    private BCryptPasswordService passwordService;

    @Inject
    private Event<AuthenticationEvent> monitoring;

    @Inject
    private AuthenticationEventMonitor authenticationEventMonitor;

    @POST
    @Path("signup")
    /**
     * NOTE: wheen test this API using Postman or other clients, remember to set the
     * content type in the header as application/x-www-form-urlencoded and
     * user x-www-form-urlencoded for the body instead of form-data!
     */
    public Response signUp(@NotNull @FormParam("username") String username,
                           @NotNull @FormParam("password") String password,
                           @NotNull @FormParam("email") String email,
                           @Context HttpServletRequest request) {
        if (userService.findByUsername(username) != null) {
            return Response.serverError().entity("Username already registered").build();
        }
        if (userService.findByEmail(email) != null) {
            return Response.serverError().entity("Email already registered").build();
        }

        User u = new User();
        u.setEmail(email);
        u.setUsername(username);
        u.setPassword(passwordService.encryptPassword(password));
        u = userService.save(u);
        logger.log(Level.INFO, "User " + u.getId() + " saved");
        return Response.ok().build();
    }

    @POST
    @Path("login")
    public Response login(@NotNull @FormParam("username") String username,
                          @NotNull @FormParam("password") String password,
                          @NotNull @FormParam("rememberMe") boolean rememberMe,
                          @Context HttpServletRequest request) {

        try {
            SecurityUtils.getSubject().login(new UsernamePasswordToken(username, password, rememberMe));
        } catch (Exception e) {
            throw new IncorrectCredentialsException("Unknown user, please try again");
        }
        monitoring.fire(new AuthenticationEvent(username, AuthenticationEvent.Type.LOGIN));
        String token = userService.issueToken(username);
        return Response.ok(new JwtToken(token)).build();
    }

    @GET
    @Path("logout")
    public Response logout(@Context HttpServletRequest request) {
        SecurityUtils.getSubject().logout();

        return this.getRedirectResponse(WebPages.HOME_URL, request);
    }


    @GET
    @Path("me")
    @ApiOperation(value = "Get self information", response = User.class)
    public Response getSubjectInfo() {
        Subject subject = SecurityUtils.getSubject();
        if (subject != null && subject.isAuthenticated()) {
            User connectedUser = this.userService.findByUsername(subject.getPrincipal().toString());
            return Response.ok(connectedUser).build();
        } else {
            return Response.serverError()
                    .type(MediaType.TEXT_HTML)
                    .build();
        }
    }

    @GET
    @Path("users")
    @ApiOperation(value = "Get online users", response = User.class, responseContainer = "List")
    public Response getConnectedUsers() {
        List<String> connectedUsers = this.authenticationEventMonitor.getConnectedUsers();
        return Response.ok(connectedUsers).build();
    }


    private Response getRedirectResponse(String requestedPath, HttpServletRequest request) {
        String appName = request.getContextPath();

        String baseUrl = request.getRequestURL().toString().split(request.getRequestURI())[0] + appName;

        try {
            if (!appName.isEmpty() && requestedPath.split(appName).length > 1) {
                baseUrl += requestedPath.split(appName)[1];
            } else {
                baseUrl += requestedPath;
            }
            logger.log(Level.INFO, "Redirecting to " + baseUrl);

            return Response.seeOther(new URI(baseUrl)).build();
        } catch (URISyntaxException ex) {
            logger.log(Level.WARNING, "URL redirection failed for request[{0}] : {1}", new Object[]{request, ex});
            return Response.serverError().status(404)
                    .type(MediaType.TEXT_HTML).build();
        }
    }

}