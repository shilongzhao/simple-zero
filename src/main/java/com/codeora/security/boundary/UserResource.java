package com.codeora.security.boundary;


import com.codeora.security.control.UserService;
import com.codeora.security.entity.User;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

/**
 */
@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(tags = "users")
@ShiroSecured
public class UserResource {
    @Inject
    private UserService userService;

    @POST
    public Response addUser(@Valid User user, @Context UriInfo info) {
        User saved = this.userService.save(user);
        long id = saved.getId();
        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).build();
    }

    @DELETE
    @Path("{id}")
    @RequiresPermissions("users:delete")
    public Response deleteUser(@PathParam("id") long id) {
        this.userService.delete(id);
        return Response.ok().build();

    }

    @PUT
    public Response editUser(@Valid User user, @Context UriInfo info) {
        User searched = this.userService.save(user);
        return Response.ok(searched).build();

    }

    @GET
    @Path("{id}")
    @RequiresPermissions("non-exist:perm")
    public Response findUser(@PathParam("id") long id) {
        User searched = this.userService.findById(id);
        if (searched == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(searched).build();

    }

    @GET
    @RequiresPermissions("users:read")
    public Response listUsers() {
        List<User> all = this.userService.findAll();
        return Response.ok(all).build();
    }
}
