package com.codeora.configuration;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class ConfigurationService {
    @PersistenceContext
    private EntityManager em;
    @Inject
    private Logger logger;

    public String getValue(ConfigurationVariable v) {
        ConfigurationVariable p = null;
        try {
             p = em.createQuery(" " +
                    " SELECT v FROM ConfigurationVariable v " +
                    " WHERE v.name = :name ", ConfigurationVariable.class)
                    .setParameter("name", v.getName())
                    .getSingleResult();
        } catch (NoResultException e) {
            logger.log(Level.INFO, "no configuration found in DB " + v.getName());
        }
        return (p == null)? v.getValue(): p.getValue();
    }
}
