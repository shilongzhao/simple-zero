package com.codeora.configuration;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "configuration_variable")
public class ConfigurationVariable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @NotNull @Column(name = "name", nullable = false, unique = true)
    private String name;

    @NotNull @Column(name = "value")
    private String value;

    @NotNull @Column(name = "value_type")
    private String type;


    public ConfigurationVariable(String name, String value, String type) {
        this.name = name;
        this.value = value;
        this.type = type;
    }

    public ConfigurationVariable() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * The method is set to package access, because we want to limit access the value
     * only through the service, which will also check database.
     * @return
     */
    String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
