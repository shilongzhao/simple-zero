package com.codeora.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface QueryCondition {
    String target() default "Unknown";
    boolean nullable() default true;
    QueryOperator operator() default QueryOperator.EQUAL;
}
