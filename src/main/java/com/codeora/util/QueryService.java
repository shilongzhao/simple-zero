package com.codeora.util;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class QueryService {
    @PersistenceContext
    private EntityManager em;

    //TODO: optimize this method, considering split into multiple methods
    public <T> List<T> list(QueryWrap<T> query, Class<T> entityClass) throws IllegalAccessException {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);

        Root<T> root = cq.from(entityClass);

        List<Predicate> whereConditions = new ArrayList<>();

        Field[] fields = query.getClass().getDeclaredFields();
        for (Field f: fields) {
            if (!f.isAnnotationPresent(QueryCondition.class)) {
                continue;
            }

            QueryCondition a = f.getAnnotation(QueryCondition.class);
            f.setAccessible(true);

            if (a.nullable() && f.get(query) == null) {
                continue;
            }

            //TODO: add more operators and filters: LIKE, IN, JOIN, JOIN FETCH, HAVING, etc.
            switch (a.operator()) {
                case EQUAL:
                    Predicate eq = cb.equal(root.get(a.target()), f.get(query));
                    whereConditions.add(eq);
                    break;
                case GE:
                    Predicate ge = cb.ge(root.get(a.target()), (Number) f.get(query));
                    whereConditions.add(ge);
                    break;
                case LE:
                    Predicate le = cb.le(root.get(a.target()), (Number) f.get(query));
                    whereConditions.add(le);
                    break;
                default:
                    throw new RuntimeException("Unknown MySQL operation in system: " + a.operator());
            }
        }
        cq.where(whereConditions.toArray(new Predicate[whereConditions.size()]));

        return em.createQuery(cq).getResultList();
    }
}
