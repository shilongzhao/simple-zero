package com.codeora;


import com.codeora.apartments.boundary.ApartmentResource;
import com.codeora.security.RESTCorsDemoRequestFilter;
import com.codeora.security.RESTCorsDemoResponseFilter;
import com.codeora.security.boundary.AuthenticationResource;
import com.codeora.security.boundary.UserResource;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * JAX-RS activator and Swagger configurator 
 */
@ApplicationPath("api")
public class JaxRsConfiguration extends Application {

    /**
     * configure Swagger
     */
    public JaxRsConfiguration() {
        super();
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("2.0");
        beanConfig.setTitle("Swagger Auto Generated API");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8180");
        beanConfig.setBasePath("/api");
        beanConfig.setResourcePackage("com.codeora");
        beanConfig.setScan(true);
    }

    /**
     * Register Swagger to RestEasy
     * @return
     */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        resources.addAll(super.getClasses());
        resources.add(ApiListingResource.class);
        resources.add(SwaggerSerializers.class);
        resources.add(UserResource.class);
        resources.add(AuthenticationResource.class);
        resources.add(ApartmentResource.class);

        resources.add(RESTCorsDemoRequestFilter.class);
        resources.add(RESTCorsDemoResponseFilter.class);
        return resources;
    }
}